package com.service;

import com.mapper.BlogMapper;
import com.pojo.*;

import java.util.*;

public class BlogServiceImpl implements BlogService {
    private BlogMapper blogMapper;

    public void setBlogMapper(BlogMapper blogMapper) {
        this.blogMapper = blogMapper;
    }

    @Override
    public String releaseBlog(Map<String, Object> map) {
        Blog blog = new Blog();
        String type =(String) map.get("type");
        BlogType blogType = queryTypeByName(type);
        String bid = UUID.randomUUID().toString().replaceAll("-", "");
        int editType=Integer.parseInt((String)map.get("editType"));

        blog.setBid(bid);
        blog.setTid(blogType.getTid());
        blog.setTitle((String) map.get("title"));
        blog.setArticle((String) map.get("article"));
        blog.setEditorType(editType);

        blogMapper.releaseBlog(blog);
        return bid;
    }

    @Override
    public void insertLabel(Map<String, Object> map) {
        BlogLabel blogLabel = new BlogLabel();
        String obj = (String) map.get("labelNum");
        int len = Integer.parseInt(obj);

        Map<String,String> param = new HashMap<>();

        for (int i = 0; i < len; i++) {
            String lid = UUID.randomUUID().toString().replaceAll("-", "");
            blogLabel.setLabelName((String)map.get("label["+i+"]"));
            blogLabel.setLid(lid);
            try {
                blogMapper.insertLabel(blogLabel);
            } catch (Exception e){
                //存在相同的标签则查询出对应的id
                lid = blogMapper.queryLidByName(blogLabel.getLabelName());
            } finally{
                param.put("bid",map.get("bid").toString());
                param.put("lid",lid);
                blogMapper.insertLabelBlog(param);
                continue;
            }
        }
    }

    @Override
    public void insertLabel(String label) {
        System.out.println("========"+label);
        BlogLabel blogLabel = new BlogLabel();
        String lid = UUID.randomUUID().toString().replaceAll("-","");
        blogLabel.setLid(lid);
        blogLabel.setLabelName(label);
        blogMapper.insertLabel(blogLabel);
    }

    @Override
    public String queryLidByName(String label) {
        String lid = blogMapper.queryLidByName(label);
        return lid;
    }

    @Override
    public List<BlogType> queryAllBlogType(int start) {
        List<BlogType> blogTypes = blogMapper.queryAllBlogType(start);
        return blogTypes;
    }

    @Override
    public List<BlogLabel> queryAllBlogLabel(int start) {
        List<BlogLabel> blogLabels = blogMapper.queryAllBlogLabel(start);
        return blogLabels;
    }

    @Override
    public List<BlogLabel> queryHotBlogLabel() {
        List<BlogLabel> blogLabels = blogMapper.queryHotBlogLabel();
        return blogLabels;
    }

    @Override
    public List<Blog> queryAllBlogMessage(int start) {
        List<Blog> blogList = blogMapper.queryAllBlogMessage(start);
        return blogList;
    }

    @Override
    public int countBlogNumber() {
        int i = blogMapper.countBlogNumber();
        return i;
    }

    @Override
    public int countBlogNumberByTid(String tid) {
        int i = blogMapper.countBlogNumberByTid(tid);
        return i;
    }

    @Override
    public void updateBlog(Map<String, Object> map) {
        String type = (String) map.get("type");
        int editType = Integer.parseInt((String) map.get("editType"));
        BlogType blogType = queryTypeByName(type);
        Blog blog = new Blog();
        blog.setBid(map.get("bid").toString());
        blog.setTitle(map.get("title").toString());
        blog.setArticle(map.get("article").toString());
        blog.setTid(blogType.getTid());
        blog.setEditorType(editType);
        blogMapper.updateBlog(blog);
    }

    @Override
    public void updateBlogLabel(Map<String, Object> map) {
        //将相关博客的标签全部删除
        blogMapper.deleteBlogLabel(map.get("bid").toString());
        insertLabel(map);
    }

    @Override
    public void deleteBlog(Map<String,String> bids) {
        Collection<String> values = bids.values();
        List<String> list = new ArrayList<>(values);
        for (String bid : list) {
            blogMapper.deleteBlog(bid);
            blogMapper.deleteBlogLabel(bid);
            blogMapper.deleteCommentByBid(bid);
        }
    }

    @Override
    public void countAllLabel() {
        List<BlogLabel> blogLabels = blogMapper.queryAllBlogLabel(-1);
        for (BlogLabel blogLabel : blogLabels) {
            blogMapper.countAllLabel(blogLabel.getLid());
        }
    }

    @Override
    public void countAllType() {
        List<BlogType> blogTypes = blogMapper.queryAllBlogType(-1);
        for (BlogType blogType : blogTypes) {
            blogMapper.countAllType(blogType.getTid());
        }
    }

    @Override
    public void updateLabel(BlogLabel blogLabel) {
        blogMapper.updateLabel(blogLabel);
    }

    @Override
    public void deleteLabel(String lid) {
        blogMapper.deleteLabel(lid);
    }

    @Override
    public void insertType(String type) {
        String tid = UUID.randomUUID().toString().replaceAll("-", "");
        BlogType blogType = new BlogType();
        blogType.setTid(tid);
        blogType.setTypeName(type);
        blogMapper.insertType(blogType);
    }

    @Override
    public void updateType(Map<String,String> map) {
        BlogType blogType = new BlogType();
        blogType.setTid(map.get("tid"));
        blogType.setTypeName(map.get("typeName"));
        blogMapper.updateType(blogType);
    }

    @Override
    public void deleteType(String type) {
        blogMapper.deleteType(type);
    }

    @Override
    public BlogType queryTypeByName(String type) {

        BlogType blogType = blogMapper.queryTypeByName(type);
        return blogType;
    }

    @Override
    public List<Blog> queryBlogMessageByTid(String tid,int start) {
        Map<String,Object> map = new HashMap<>();
        map.put("tid",tid);
        map.put("start",start);
        List<Blog> blogList = blogMapper.queryBlogMessageByParam(map);
        return blogList;
    }

    @Override
    public List<Blog> queryBlogMessageByLid(String lid,int start) {
        Map<String,Object> map = new HashMap<>();
        List<Blog> blogList = new ArrayList<>();
        map.put("lid",lid);
        map.put("start",start);
        blogList.addAll(blogMapper.queryBlogMessageByParam(map));
        return blogList;
    }

    @Override
    public void insertComment(Comment comment) {
        String cid = UUID.randomUUID().toString().replace("-", "");
        comment.setCid(cid);
        if (comment.getLevel()>=1){
            comment.setLevel(comment.getLevel()+1);
        }
        blogMapper.insertComment(comment);
    }

    @Override
    public List<Node> queryCommentByBid(String bid) {
        List<Comment> comments = blogMapper.queryCommentByBid(bid);

        List<Node> children = new ArrayList<>();
        List<Node> nodeList = new ArrayList<>();
        for (Comment comment : comments) {
            children.add(buildNode(comment));
        }

        for (Node child : children) {
            if (child.getPid()!=null) {
                Node node = findNode(children, child.getPid());
                List<Node> nodes = new ArrayList<>();
                if (node.getChildren()!=null){
                    nodes=node.getChildren();
                }
                nodes.add(child);
                node.setChildren(nodes);
                nodeList.add(child);
            }
        }

        for (Node node : nodeList) {
            children.remove(node);
        }
        return children;
    }

    private Node findNode(List<Node> children, String id){
        for (Node child : children) {
            if (child.getCid().equals(id)){
                return child;
            }
        }
        return null;
    }

    private Node buildNode(Comment comment){
        Node node = new Node();
        node.setBid(comment.getBid());
        node.setCid(comment.getCid());
        node.setLikeNum(comment.getLikeNum());
        node.setPid(comment.getPid());
        node.setContent(comment.getContent());
        node.setPublishTime(comment.getPublishTime());
        node.setUsername(comment.getUsername());
        node.setPusername(comment.getPusername());
        node.setLevel(comment.getLevel());
        node.setChildren(null);

        return node;
    }

    @Override
    public int countCommentsNum(String bid) {
        int i = blogMapper.countCommentsNum(bid);
        return i;
    }

    @Override
    public Map<String,Object> searchBlogByKeyword(String keyword,int start) {
        String[] strings = keyword.split(" ");
        List<String> keywords=Arrays.asList(strings);

        List<Blog> blogList = blogMapper.searchBlogByKeyword(keywords,start);
        int i = blogMapper.countBlogByKeyword(keywords);

        Map<String,Object> map = new HashMap<>();
        map.put("blogList",blogList);
        map.put("count",i);
        return map;
    }

    @Override
    public void updateBlogReads(String bid, int reads) {
        blogMapper.updateBlogReads(bid, reads);
    }

    @Override
    public List<Blog> queryBlogTopTen() {
        List<Blog> blogList = blogMapper.queryBlogTopTen();
        return blogList;
    }

    @Override
    public int queryClickRateByName(String label){
        int i = blogMapper.queryClickRateByName(label);
        return i;
    }
}
