package com.service;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public interface SendEmail {
    public abstract void sendMail(String receiveMailAccount, String mailContent);

    public abstract MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail,
                                                String mailContent);
}
