package com.service;

import com.mapper.UserMapper;
import com.pojo.User;

public class UserServiceImpl implements UserService {
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User userLogin(User user) {
        User users = userMapper.userLogin(user);
        return users;
    }

    @Override
    public User queryUserByName(String username) {
        User user = userMapper.queryUserByName(username);
        return user;
    }

    @Override
    public User queryUserById(int id) {
        User user = userMapper.queryUserById(id);
        return user;
    }

    @Override
    public void register(User user) {
        userMapper.register(user);
    }

    @Override
    public String verifyUsername(String username) {
        User user = userMapper.queryUsernameExist(username);
        if (user!=null){
            return "404";
        }else {
            return "200";
        }
    }
}
