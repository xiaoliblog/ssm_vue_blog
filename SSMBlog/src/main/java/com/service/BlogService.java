package com.service;

import com.pojo.*;

import java.util.List;
import java.util.Map;

public interface BlogService {
    //发布博客
    public abstract String releaseBlog(Map<String, Object> map);

    public abstract void insertLabel(Map<String, Object> map);
    public abstract void insertLabel(String label);

    public abstract String queryLidByName(String label);

    //查询所有类型
    public abstract List<BlogType> queryAllBlogType(int start);

    //查询所有标签
    public abstract List<BlogLabel> queryAllBlogLabel(int start);

    //查询热门标签
    public abstract List<BlogLabel> queryHotBlogLabel();

    //查询所有博客的主要信息
    public abstract List<Blog> queryAllBlogMessage(int start);

    //统计博客数量
    public abstract int countBlogNumber();

    //统计同一类型的博客数量
    public abstract int countBlogNumberByTid(String tid);

    //修改博客
    public abstract void updateBlog(Map<String, Object> map);

    //更新博客标签
    public abstract void updateBlogLabel(Map<String, Object> map);

    //删除博客信息
    public abstract void deleteBlog(Map<String,String> bids);

    //统计标签使用次数
    public abstract void countAllLabel();

    //统计分类使用次数
    public abstract void countAllType();

    //更新标签
    public abstract void updateLabel(BlogLabel blogLabel);

    //删除标签
    public abstract void deleteLabel(String lid);

    //添加分类
    public abstract void insertType(String type);

    //更新分类
    public abstract void updateType(Map<String,String> map);

    //删除分类
    public abstract void deleteType(String type);

    //查询分类
    public abstract BlogType queryTypeByName(String type);

    //根据分类id查询博客
    public abstract List<Blog> queryBlogMessageByTid(String tid,int start);

    //根据标签id查询博客
    public abstract List<Blog> queryBlogMessageByLid(String lid,int start);

    //发表评论
    public abstract void insertComment(Comment comment);

    //查询当前博客的评论
    public abstract List<Node> queryCommentByBid(String bid);

    //统计博客评论数
    public abstract int countCommentsNum(String bid);

    //根据关键字搜索
    public abstract Map<String,Object> searchBlogByKeyword(String keyword,int start);

    //更新博客阅读量
    public abstract void updateBlogReads(String bid,int reads);

    //查询浏览量前十的博客
    public abstract List<Blog> queryBlogTopTen();

    public abstract int queryClickRateByName(String label);
}
