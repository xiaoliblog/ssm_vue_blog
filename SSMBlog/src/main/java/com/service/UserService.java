package com.service;

import com.pojo.User;

public interface UserService {
    public abstract User userLogin(User user);

    public abstract User queryUserByName(String username);

    public abstract User queryUserById(int id);

    public abstract void register(User user);

    public abstract String verifyUsername(String username);
}
