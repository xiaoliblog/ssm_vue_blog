package com.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pojo.Status;
import com.pojo.User;
import com.service.RsaUtil;
import com.service.SendEmailUtil;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/user")
public class UserController {
    private UserService userService;
    private RsaUtil rsaUtil;
    private ObjectMapper objectMapper = new ObjectMapper();
    private String content;

    @Autowired
    @Qualifier("userServiceImpl")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/confirmLogin")
    public String userLogin(User user, HttpServletRequest request) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpSession session = request.getSession();
        //获取查询结果
        User users = userService.userLogin(user);
        Map<String, Object> msg = new HashMap<>();

        if (users != null) {
            session.setAttribute(users.getUsername(), users);

            //设置session有效时长为一小时
            session.setMaxInactiveInterval(60 * 60);
            //System.out.println("========" + session.getAttribute("admin"));
            msg.put("status", Status.LOGIN_SUCCESS);
            msg.put("data", users);
            msg.put("tip", "恭喜，登录成功");
        } else {
            msg.put("status", Status.LOGIN_ERROR);
            msg.put("data", users);
            msg.put("tip", "用户名或密码错误");
        }

        String string = objectMapper.writeValueAsString(msg);

        return string;
    }

    @RequestMapping("/queryUserByName")
    public String queryUserByName(String username) throws JsonProcessingException {
        User user = userService.queryUserByName(username);
        String s = objectMapper.writeValueAsString(user);
        return s;
    }

    @RequestMapping("/queryUserById")
    public String queryUserById(int id) throws JsonProcessingException {
        User user = userService.queryUserById(id);
        String s = objectMapper.writeValueAsString(user);
        return s;
    }

    @RequestMapping("/checkLogin")
    public String checkLogin(int userType, HttpServletRequest request) throws JsonProcessingException {
        HttpSession session = request.getSession();
        String s = null;
        if (request.getSession(false) == null) {
            return s;
        }
        if (userType == 1) {
            User admin = (User) session.getAttribute("admin");
            s = objectMapper.writeValueAsString(admin);
        } else {
            User user = (User) session.getAttribute("user");
            s = objectMapper.writeValueAsString(user);
        }
        return s;
    }

    @RequestMapping("/getRsaKey")
    public String getRsaKey() throws JsonProcessingException {

        return null;
    }

    @RequestMapping("/emailVerify")
    public String emailVerify(String email){
        SendEmailUtil sendEmailUtil = new SendEmailUtil();
        int code = new Random().nextInt(900000)+100000;
        content = String.valueOf(code);
        String s = sendEmailUtil.sendMail(email, content);
        if(s.equals("404")){
            content=null;
        }
        return s;
    }

    @RequestMapping("/register")
    public String register(String code, User user){
        if (code.equals(content)) {
            userService.register(user);
            return "200";
        }else {
            return "404";
        }
    }

    @RequestMapping("/verifyUsername")
    public String verifyUsername(String username){
        return userService.verifyUsername(username);
    }

    @RequestMapping("/signOut")
    public void signOut(String username,HttpServletRequest request){
        HttpSession session = request.getSession();
        session.removeAttribute(username);
    }
}
