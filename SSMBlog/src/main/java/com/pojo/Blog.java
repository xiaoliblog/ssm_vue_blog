package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog {
    private int uid;
    private int sortId;
    private String bid;
    private String tid;
    private String title;
    private String article;
    private int likeNum;
    private int collectionNum;
    private String createTime;
    private String updateTime;
    private String blogLabel;
    private String blogType;
    private int reads;
    private int editorType;
}
