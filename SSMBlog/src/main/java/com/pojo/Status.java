package com.pojo;

public class Status {

    //登录用户名和密码错误
    public final static int LOGIN_ERROR=0;

    //成功登录
    public final static int LOGIN_SUCCESS=1;

    //发布博客失败
    public final static int RELEASE_ERROR=2;

    //发布博客成功
    public final static int RELEASE_SUCCESS=3;

    //删除博客失败
    public final static int DELETE_ERROR=4;

    //删除博客成功
    public final static int DELETE_SUCCESS=5;

    //标签已存在
    public final static int LABEL_EXIST=6;

    //标签不存在
    public final static int LABEL_NOEXIST=7;

    //分类已存在
    public final static int TYPE_EXIST=8;

    //分类不存在
    public final static int TYPE_NOEXIST=9;
}
