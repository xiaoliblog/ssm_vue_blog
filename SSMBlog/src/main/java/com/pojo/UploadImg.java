package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadImg {
    private Integer errno; //错误代码，0 表示没有错误。
    private List<String> data; //已上传的图片路径
}
