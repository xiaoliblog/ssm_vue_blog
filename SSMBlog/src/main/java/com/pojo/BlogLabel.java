package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogLabel {
    private String lid;
    private String labelName;
    private int clickRate;
    private String createTime;
}
