package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogType {
    private String tid;
    private String typeName;
    private int clickRate;
    private String createTime;
}
