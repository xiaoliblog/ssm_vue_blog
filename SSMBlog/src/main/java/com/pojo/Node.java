package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Node {
    private String bid;
    private int likeNum;
    private String cid;
    private String pid;
    private String content;
    private String publishTime;
    private String username;    //当前评论的用户名
    private String pusername;   //被回复评论的用户名
    private int level;
    private List<Node> children;
}
