package com.mapper;

import com.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    public abstract User userLogin(User user);

    public abstract User queryUserByName(String username);

    public abstract User queryUserById(int id);

    public abstract void register(User user);

    public abstract User queryUsernameExist(@Param("username") String username);
}
