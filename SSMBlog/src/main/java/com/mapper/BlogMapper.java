package com.mapper;

import com.pojo.Blog;
import com.pojo.BlogLabel;
import com.pojo.BlogType;
import com.pojo.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BlogMapper {
    //发布博客
    public abstract int releaseBlog(Blog blog);
    //插入标签
    public abstract int insertLabel(BlogLabel blogLabel);
    //插入博客id和标签id
    public abstract int insertLabelBlog(Map<String,String> map);
    //添加分类
    public abstract void insertType(BlogType blogType);
    //发表评论
    public abstract void insertComment(Comment comment);



    //查询插入的标签的id
    public abstract String queryLidByName(String label);
    //根据标签名查询标签使用次数
    public abstract int queryClickRateByName(@Param("label") String label);
    //查询所有类型
    public abstract List<BlogType> queryAllBlogType(int start);
    //查询所有标签
    public abstract List<BlogLabel> queryAllBlogLabel(int start);
    //查询热门标签
    public abstract List<BlogLabel> queryHotBlogLabel();
    //查询博客主要信息
    public abstract List<Blog> queryAllBlogMessage(int start);
    //查询博客信息
    public abstract List<Blog> queryBlogMessageByParam(Map<String,Object> map);
    //根据标签id查询博客id
    public abstract List<String> queryBidByLid(String lid);
    //统计博客数量
    public abstract int countBlogNumber();
    //统计同一类型的博客数量
    // （类型表中存在使用数量字段，可以修改）
    public abstract int countBlogNumberByTid(String tid);
    //统计标签使用次数
    public abstract int countAllLabel(String lid);
    //统计分类使用次数
    public abstract int countAllType(String tid);
    //根据分类名查找
    public abstract BlogType queryTypeByName(String type);
    //查询当前博客的评论
    public abstract List<Comment> queryCommentByBid(String bid);
    //统计博客评论数
    public abstract int countCommentsNum(String bid);
    //根据关键字搜索
    public abstract List<Blog> searchBlogByKeyword(@Param("list") List<String> keywords,@Param("start") int start);
    //统计搜索结果数量
    public abstract int countBlogByKeyword(List<String> keywords);
    //查询浏览量前十的博客
    public abstract List<Blog> queryBlogTopTen();



    //修改博客
    public abstract int updateBlog(Blog blog);
    //更新标签
    public abstract void updateLabel(BlogLabel blogLabel);
    //更新分类
    public abstract void updateType(BlogType blogType);
    //更新博客访问量
    public abstract void updateBlogReads(@Param("bid") String bid, @Param("reads") int reads);



    //删除对应博客的全部标签
    public abstract int deleteBlogLabel(String bid);
    //删除博客信息
    public abstract int deleteBlog(String bid);
    //删除标签
    public abstract void deleteLabel(String lid);
    //删除分类
    public abstract void deleteType(String type);
    //删除博客评论
    public abstract void deleteCommentByBid(String bid);

}
