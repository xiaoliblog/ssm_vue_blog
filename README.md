# 基于SSM＋VUE的个人博客

#### 介绍

使用ssm和vue搭建的个人博客

前端使用VUE+Element-UI

后端采用SSM

前端用vuex管理一些数据

富文本编辑器使用WangEditor

Markdown编辑器使用MavonEditor

axios进行前后端交互

目前项目已部署(有些小问题，第一次访问有时候会报错，需要刷新一次）

http://www.originsblog.cn:8080/dist/#/home/homePage

> 管理员：
> 用户名：admin
> 密码：123456

图片保存在阿里云oss上，便于管理

>   前端代码由于要部署的原因，有几个小地方发生变化
>
>   -   main.js中axios的baseURL
>
>   -   build/util.js中添加了publicPath属性
>
>       ```js
>       if (options.extract) {
>         return ExtractTextPlugin.extract({
>           use: loaders,
>           fallback: 'vue-style-loader',
>           publicPath: '../../'
>         })
>       } else {
>         return ['vue-style-loader'].concat(loaders)
>       }
>       ```
>
>   -   index.js修改了build中的assetsPublicPath
>
>   后端图片上传功能改为了阿里云OSS上传
>
>   数据库的设计原本是为了写一个多人博客系统，不过现在写了个人博客，所以有部分表是暂时用不上的

#### 目前已完成功能

-   后端管理界面
    -   博客管理
    -   标签管理
    -   分类管理
-   前端博客展示页面
    -   用户登录和注册，需要邮箱验证
    -   博客的显示
    -   简单的评论功能
    -   关键词搜索，目前只能对标题搜索，但是点击标签或者分类可以搜索对应标签或分类的内容

#### 预期计划

-   博客管理可以对博客进行筛选
-   给标签和分类添加批量删除
-   添加用户管理
-   添加评论管理
    -   博客显示页面， 登录用户可以举报不当言论，也可删除自己表的评论
    -   管理界面，处理举报信息。
-   添加OAuth2第三方登录
-   前端页面添加归档功能，按时间归档
-   博客统计访问量功能
-   博客显示代码添加行号以及一键复制
-   点击标签或者博客分类进行搜索或者其他操作在页面显示结果
-   搜索功能强化
-   样式美化
-   前端vue优化整体结构，封装接口。后端ssm改spring boot简化配置，优化sql等



其实有挺多想写的，不过能力有限，只能一点点慢慢完善，其实开始是打算做类似博客园、csdn的论坛，不过发现太复杂，想想就头大，于是就做了个人博客。

