import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login";
import Home from "../views/Home";
import Manager from "../views/Manager";
import BlogManager from "../components/after/blog/BlogManager";
import Test from "../views/Test";
import LabelManager from "../components/after/label/LabelManager";
import TypeManager from "../components/after/type/TypeManager";
import ShowByType from "../components/front/type/ShowByType";
import ShowBlog from "../components/front/blog/ShowBlog";
import ShowByLabel from "../components/front/label/ShowByLabel";
import HomePage from "../components/front/home/HomePage";
import ShowSearchResults from "../components/front/search/ShowSearchResults";
import Register from "../views/Register";

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/login',
      component: Login,
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/home',
      component: Home,
      meta: {
        home: true
      },
      children: [
        {
          path: '/blog/showBlog',
          component: ShowBlog,
          meta: {
            userLogin: true
          },
        },
        {
          path: '/type/showByType',
          component: ShowByType,
          meta: {
            userLogin: true
          },
        },
        {
          path: '/label/showByLabel',
          component: ShowByLabel,
          meta: {
            userLogin: true
          },
        },
        {
          path: '/home/homePage',
          component: HomePage,
          meta: {
            userLogin: true
          },
        },
        {
          path: '/blog/search',
          component: ShowSearchResults,
          meta: {
            userLogin: true
          },
        }
      ]
    },
    {
      path: '/manager',
      component: Manager,
      meta: {
        adminLogin: true
      },
      children: [
        {
          path: '/blog/blogManager',
          component: BlogManager,
          meta: {
            adminLogin: true
          },
        },
        {
          path: '/label/labelManager',
          component: LabelManager,
          meta: {
            adminLogin: true
          },
        },
        {
          path: '/type/typeManager',
          component: TypeManager,
          meta: {
            adminLogin: true
          },
        }
      ]
    },
    {
      path: '/test',
      component: Test
    }
  ]
})
