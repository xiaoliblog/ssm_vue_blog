// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';
import VueAxios from 'vue-axios';
import qs from 'qs';
import {HappyScroll} from 'vue-happy-scroll';
import 'vue-happy-scroll/docs/happy-scroll.css';
import Vuex from 'vuex';
import highlight from 'highlight.js';
import 'font-awesome/css/font-awesome.min.css';
import VueRouter from "vue-router";
import JsEncrypt from 'jsencrypt';
import router from './routers'

Vue.component('happy-scroll', HappyScroll);

axios.defaults.withCredentials = true;
axios.defaults.baseURL = '/Test_war_exploded';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
Vue.config.productionTip = false;
Vue.use(VueRouter)
Vue.use(ElementUI);
Vue.use(VueAxios, axios);
Vue.use(Vuex);
Vue.use(qs);

Vue.prototype.bus = new Vue();
Vue.prototype.$jsEncrypt = JsEncrypt;


Vue.directive('highlight', (el) => {
  let blocks = el.querySelectorAll('pre code')
  blocks.forEach((block) => {
    highlight.highlightBlock(block);
  })
})

router.beforeEach((to, from, next) => {
  if (to.meta.adminLogin){
    let item = JSON.parse(localStorage.getItem("admin"));
    if (item===null){
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    }else {
      axios.post('/blog/interceptor', qs.stringify({
        username: item.username
      })).then((response) => {
        if (response.data) {
          next();
        } else {
          localStorage.removeItem("admin");
          next({
            path: '/login',
            query: {
              redirect: to.fullPath
            }
          })
        }
      })
    }
  }else {
    next();
  }
  if (to.meta.home){
    next({
      path:'/home/homePage'
    })
  }
  if (to.meta.userLogin){
    let item = JSON.parse(localStorage.getItem("user"));
    if (item!==null){
      axios.post('/blog/interceptor', qs.stringify({
        username: item.username
      })).then((response) => {
        if (response.data) {
          next();
        } else {
          localStorage.removeItem("user");
          window.location.reload();
        }
      })
    }else {
      next();
    }
  }
})

const store = new Vuex.Store({
  state: {
    blogDetail: {},
    user: {
      username: '',
      status: false,
      avatar: 'https://origins-blog.oss-cn-beijing.aliyuncs.com/default.png',
    },
    admin: {
      username: '',
      status: false,
      avatar: 'https://origins-blog.oss-cn-beijing.aliyuncs.com/default.png'
    },
    replayId: '-1',

  },
  mutations: {
    setBlog(state, blog) {
      state.blogDetail = blog;
    },
    setUser(state, user) {
      state.user.username = user.username;
      state.user.status = user.status;
      state.user.avatar = user.avatar;
    },
    signOut(state) {
      state.user.username = '';
      state.user.status = false;
      state.user.avatar = 'https://origins-blog.oss-cn-beijing.aliyuncs.com/default.png';
    },
    setAdmin(state, admin) {
      state.admin.username = admin.username;
      state.admin.status = admin.status;
      state.admin.avatar = admin.avatar;
    },
    signOutAdmin(state) {
      state.admin.username = '';
      state.admin.status = false;
      state.admin.avatar = 'https://origins-blog.oss-cn-beijing.aliyuncs.com/default.png';
    },
    setReplayId(state, id) {
      state.replayId = id;
    },
    reloadReplayId(state) {
      state.replayId = '-1';
    },
  },
  getters: {
    getBlogDetail: (state) => {
      return state.blogDetail;
    },
    getUser: (state) => {
      return state.user;
    },
    getReplayId: (state) => {
      return state.replayId;
    },
    getAdmin: (state) => {
      return state.admin;
    }
  }
})

new Vue({
  el: '#app',
  router,
  store,
  data() {
    return {value: ''}
  },
  render: h => h(App)
})
